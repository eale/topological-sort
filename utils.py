def build_graph(matrix_str):
    matrix_list = [[int(elem) for elem in x.split(',')] for x in matrix_str.split("\n")]
    matrix_dict = {x: [] for x in range(1, len(matrix_list) + 1)}

    for i in range(len(matrix_list)):
        for j in range(len(matrix_list[i])):
            if matrix_list[i][j] == 1:
                matrix_dict[i + 1].append(j)

    return matrix_dict


def build_graph_from_path(path):
    with open(path) as file:
        matrix_str = file.read()

    return build_graph(matrix_str)


def main():
    build_graph("load_testing_data/101_2.txt")


if __name__ == '__main__':
    main()

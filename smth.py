import networkx as nx
from random import randint



def random_dag(nodes: int, edges: int):
    """Generate a random Directed Acyclic Graph (DAG) with a given number of nodes and edges."""
    G = nx.DiGraph()
    for i in range(nodes):
        G.add_node(i)
    while edges > 0:
        a = randint(0, nodes-1)
        b = a
        while b == a:
            b = randint(0, nodes-1)
        G.add_edge(a, b)
        if nx.is_directed_acyclic_graph(G):
            edges -= 1
        else:
            # we closed a loop!
            G.remove_edge(a, b)
    return G


if __name__ == '__main__':
    result = (nx.adjacency_matrix(random_dag(6, 7)).todense().tolist())

    print(result)

import csv
from topological_sort import topological
import datetime
import os
import statistics
import time
from utils import build_graph_from_path


def generate_test_results_template():
    test_data_path = os.path.join(os.getcwd(), 'load_testing_data')

    if not os.path.isdir(test_data_path):
        print('No test data found!')
        return

    test_results = {}

    for test_file_name in os.listdir(test_data_path):
        size, num = test_file_name.split('.')[0].split('_')
        if size not in test_results:
            test_results[size] = {}
        test_results[size][num] = None

    return test_results


def fill_test_results(test_results):
    test_data_path = os.path.join(os.getcwd(), 'load_testing_data')

    for size in test_results:
        for num in test_results[size]:
            graph = build_graph_from_path(os.path.join(test_data_path, f'{size}_{num}.txt'))

            test_start_time = datetime.datetime.now()
            topological(graph)
            test_end_time = datetime.datetime.now()

            test_results[size][num] = (test_end_time - test_start_time).microseconds

    return test_results


def process_test_results(test_results):
    tests_info = {}

    for size, results in test_results.items():
        tests_info[size] = {
            'min': min(results.values()),
            'max': max(results.values()),
            'avg': statistics.mean(results.values()),
            'median': statistics.median(results.values()),
        }

    return tests_info


def create_csv_file(tests_info):
    test_measurements_path = os.path.join(os.getcwd(), 'load_testing_measurements')

    if not os.path.isdir(test_measurements_path):
        os.mkdir(test_measurements_path)

    with open(os.path.join(test_measurements_path, f'{int(time.time())}.csv'), mode='w+', encoding='utf-8') as f:
        writer = csv.writer(f)
        writer.writerow(['size', 'min', 'max', 'avg', 'median'])

        for size, info in sorted(tests_info.items(), key=lambda x: int(x[0])):
            writer.writerow([size] + [info[key] for key in ['min', 'max', 'avg', 'median']])


def measure_time():
    test_results = fill_test_results(generate_test_results_template())
    create_csv_file(process_test_results(test_results))


def main():
    measure_time()


if __name__ == '__main__':
    main()

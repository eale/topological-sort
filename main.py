import click
import tests
import visual
import measure_time
from topological_sort import topological
import utils


@click.group()
def main():
    pass


@main.command()
@click.option('--data', default=None)
def process_one(data: str) -> None:
    graph = utils.build_graph(data)
    result = topological(graph)
    print(result)


@main.command()
@click.option('--start', default=None, type=int)
@click.option('--step', default=None, type=int)
@click.option('--end', default=None, type=int)
@click.option('--count', default=None, type=int)
def generate_data(start: int, end: int, step: int, count: int) -> None:
    tests.generate_data(start, end, step, count)


@main.command()
@click.option('--file', default="chart.csv")
def create_chart(file: str) -> None:
    visual.create_chart(file)


@main.command()
def measure_algo() -> None:
    measure_time.measure_time()


if __name__ == '__main__':
    main()

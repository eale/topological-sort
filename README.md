# topological sort

- Итак как же это дело у нас запускается
    - Для начала установитье все зависимости командой poetry install
    - активируем виртуальное окружение
- запускаем генерацию тестов командой 
    - если скачано с гитлаба
    ```
    python main.py generate-data --start 1 --end 22 --step 10 --count 5 
    ```

    - если скачано как пакет 
     ```
    sw_2_t_7 generate-data --start 1 --end 22 --step 10 --count 5 
    ```

- генерируем csv файл с данными тестов
    - если скачано с гитлаба
    ```
    python main.py measure-algo
    ```

    - если скачано как пакет 
    ```
    sw_2_t_7 measure-algo
    ```
  
- генерируем график с данными
    - если скачано с гитлаба
    ```
    python main.py create-chart --file load_testing_measurements/1620506940.csv 
    ```
    - если скачано как пакет 
     ```
    sw_2_t_7 create-chart --file load_testing_measurements/1620506940.csv 
    ```

Вот так и работает замечательный алгоритм топологической сортировки. И вот график показывающий его временную сложность, подробнее про алгоритм можно почитать [тут](https://docs.google.com/document/d/1zcr7K0JCLFfolq3O0XfkEksJjx7zNRsIZIHZ2Afy51o/edit?usp=sharing)
import os
from math import sqrt, ceil
from random import randint
import networkx as nx
from random import randint


def generate_matrix_graph(nodes: int, edges: int):
    G = nx.DiGraph()
    for i in range(nodes):
        G.add_node(i)
    while edges > 0:
        a = randint(0, nodes-1)
        b = a
        while b == a:
            b = randint(0, nodes-1)
        G.add_edge(a, b)
        if nx.is_directed_acyclic_graph(G):
            edges -= 1
        else:
            # we closed a loop!
            G.remove_edge(a, b)

    list_graph = nx.adjacency_matrix(G).todense().tolist()

    result = '\n'.join(','.join(str(i) for i in x) for x in list_graph)

    if not list_graph:
        result = "-1"

    return result


def generate_data(start: int, end: int, step: int, count: int):
    print(start, end, step, count)
    directory = "load_testing_data"

    if not os.path.exists(directory):
        os.mkdir(directory)

    for i in range(start, end, step):
        print(i)
        test_index = 1
        for j in range(count):
            file_name = f"{i}_{test_index}.txt"
            file_path = os.path.join(directory, file_name)

            with open(file_path, mode="w+") as file:
                matrix_graph = generate_matrix_graph(i, i+i)

                file.write(matrix_graph)

            test_index += 1


def main():
    generate_data(1, 10, 22, 5)


if __name__ == '__main__':
    main()

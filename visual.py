import pandas as pd
import matplotlib.pyplot as plt
import os
import re


def create_chart(csv_path):
    """Yes, it's shit code, but I don't give a fuck, it works perfect"""
    headers = ['size', 'min', 'max', 'avg', 'median']
    _, ax = plt.subplots()
    df = pd.read_csv(csv_path, names=headers)
    plt.style.use('ggplot')
    x = list(df['size'])
    avg = list(df['avg'])
    min = list(df['min'])
    max = list(df['max'])
    median = list(df['median'])
    x = x[1:]
    min = min[1:]
    max = max[1:]
    median = median[1:]
    avg = avg[1:]
    x = [int(i) for i in x]
    avg = [float(i) for i in avg]
    min = [int(i) for i in min]
    max = [int(i) for i in max]
    median = [float(i) for i in median]
    plt.xlabel('test number', color='gray')
    plt.ylabel('avarage time', color='gray')
    ax.plot(x, avg, label="avg")
    ax.plot(x, min, label="min")
    ax.plot(x, max, label="max")
    ax.plot(x, median, label="median")
    ax.legend(fontsize=10,
              ncol=2,  # количество столбцов
              facecolor='lightgray',  # цвет области
              edgecolor='black',  # цвет крайней линии
              title='Прямые',  # заголовок
              title_fontsize='10'  # размер шрифта заголовка
              )
    try:
        os.mkdir('load_testing_plots')
    except Exception:
        pass

    plt.draw()
    name = re.findall(r'(/(.+).csv)|((.+).csv)', csv_path)
    plt.savefig(f"load_testing_plots/{name[0][3]}.jpg")

from collections import defaultdict, deque
import click
from utils import build_graph_from_path


def topological(graph: dict):
    gray = 0
    black = 1

    stack = deque()
    enter = set(graph)
    state = dict()

    def dfs(node):
        state[node] = gray
        for key in graph.get(node, ()):
            color_of_k = state.get(key, None)
            if color_of_k == gray:
                raise ValueError
            if color_of_k == black:
                continue
            enter.discard(key)
            dfs(key)
        stack.appendleft(node)
        state[node] = black

    while enter:
        try:
            dfs(enter.pop())
        except ValueError:
            return -1

    return list(stack)


def main():
    graph = build_graph_from_path("load_testing_data/21_3.txt")
    result = topological(graph)
    print(result)


if __name__ == '__main__':
    main()

from topological_sort import topological


def test1():
    graph = {
        1: [2, 3],
        2: [4, 5, 6],
        3: [4, 6],
        4: [5, 6],
        5: [6],
        6: []
    }
    assert topological(graph) == [1, 3, 2, 4, 5, 6]


def test2():
    graph = {
        1: [2],
        2: [5],
        3: [2],
        4: [6],
        5: [],
        6: [5]
    }
    assert topological(graph) == [4, 6, 3, 1, 2, 5]


def test3():
    graph = {
        1: [2],
        2: [1]
    }
    assert topological(graph) == -1


def test4():
    graph = {
        1: [2, 3, 4],
        2: [4],
        3: [4],
        4: []
    }
    assert topological(graph) == [1, 3, 2, 4]


def test5():
    graph = {
        1: [2, 3],
        2: [4],
        3: [4],
        4: [3]
    }
    assert topological(graph) == -1


def test6():
    graph = {
        1: [2, 5],
        2: [3],
        3: [],
        4: [1, 5],
        5: [2, 3]
    }
    assert topological(graph) == [4, 1, 5, 2, 3]
